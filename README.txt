Part 1

The code is run by:
python3 ex1

Input files:
microblog2011.txt -> input tweets
StopWords.txt -> stopwords in each line (+http)

It creates these files:
microblog2011_tokenized.txt -> tokenized output
Tokens.txt -> frequency of tokens (sorted)
Words.txt -> frequency of words (sorted)
WordsWithoutStops.txt -> frequency of words without stopwords (sorted)
BiWords.txt -> frequency of biwords without stopwords (sorted)

It also prints lexical diversity and token type ration and the counts as well.

Part 2

This task requires word embeddings benchmarks library. It also requires gensim and bpemb which can be installed using pip.

To run the task:
python3 evaluate_similarity.py

dm.vec is the CBOW embedding we trained by ourselves. It can be downloaded from https://bitbucket.org/nokhbeh100/nlp-ex/src/master/ass1/dm.vec