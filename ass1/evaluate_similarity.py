# -*- coding: utf-8 -*-

"""
 Simple example showing evaluating embedding on similarity datasets
"""
import logging
from six import iteritems
from web.datasets.similarity import *
from web.embedding import Embedding
from web.embeddings import *
from web.analogy import *
from web.evaluate import *
from bpemb import BPEmb as b
import scipy

# Configure logging
logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG, datefmt='%I:%M:%S')

# Fetch GloVe embedding (warning: it might take few minutes)
#w_glove = fetch_GloVe(corpus="wiki-6B", dim=300)

embeddings = [
    #[fetch_FastText(),"HPCA"],
    #[fetch_morphoRNNLM("CW"),"RNNLM"],
    #[fetch_NMT(),"NMT"],


    [fetch_SG_GoogleNews(),"Skip Gram"],

    [fetch_conceptnet_numberbatch(),"Conceptnet"],
    [fetch_GloVe(corpus="wiki-6B", dim=300),"GloVe"],
    [fetch_LexVec(),"LexVec"]
    [fetch_PDC(),"PDC"],
    [fetch_HDC(),"HDC"],
    [Embedding.from_word2vec('dm.vec'), "CBOW"],

]

Define tasks
tasks = {
    "MEN": fetch_MEN(),
    "WS353": fetch_WS353(),
    "SIMLEX999": fetch_SimLex999(),

    "MTurk": fetch_MTurk(),
    "Rubenstein & Goodenough": fetch_RG65(),
    "RareWords": fetch_RW(),
    "TR9856": fetch_TR9856()

}

analogy_tasks = {
    
    "Google analogy": fetch_google_analogy(),
    "MSR": fetch_msr_analogy(),
}

Print sample data
for name, data in iteritems(tasks):
   print("Sample data from {}: pair \"{}\" and \"{}\" is assigned score {}".format(name, data.X[0][0], data.X[0][1], data.y[0]))

for embedding, ename in embeddings:
   # Calculate results using helper function
   for name, data in iteritems(tasks):
       print("Spearman correlation of scores on {}, embedding {} {}".format(name, ename, evaluate_similarity(embedding, data.X, data.y)))


w = b(lang="en", dim=100, vs=100000)

for name, data in iteritems(tasks):
    mean_vector = np.mean(w.vectors, axis=0, keepdims=True)
    A = np.vstack(w.embed(word)[0] for word in data.X[:, 0])
    B = np.vstack(w.embed(word)[0] for word in data.X[:, 1])

    #print(A.shape)
    #print(B.shape)
    scores = np.array([v1.dot(v2.T)/(np.linalg.norm(v1)*np.linalg.norm(v2)) for v1, v2 in zip(A, B)])
    kk = scipy.stats.spearmanr(scores, data.y).correlation

    print("Spearman correlation of scores on {}, embedding {} {}".format(name, "BPEmb", kk))


w = Embedding.from_word2vec('dm.vec')

for name, data in iteritems(tasks):
    X = data.X
    y = data.y
    mean_vector = np.mean(w.vectors, axis=0, keepdims=True)
    A = np.vstack(w.get(word, mean_vector) for word in X[:, 0])
    B = np.vstack(w.get(word, mean_vector) for word in X[:, 1])
    scores = np.array([v1.dot(v2.T)/(np.linalg.norm(v1)*np.linalg.norm(v2)) for v1, v2 in zip(A, B)])
    kk = scipy.stats.spearmanr(scores, data.y).correlation
    print("Spearman correlation of scores on {}, embedding {} {}".format(name, "CBOW", kk))

for name, data in iteritems(analogy_tasks):
    X = data.X
    y = data.y
    for embedding, ename in embeddings:
        solver = SimpleAnalogySolver(w=embedding, method="add", batch_size=100, k=None)
        y_pred = solver.predict(X)
        kk = np.mean(y_pred == y)
        print("Analogy prediction accuracy on {}, embedding {} {}".format(name, ename, kk))

for embedding, ename in embeddings:
    
    kk = evaluate_on_WordRep(embedding, max_pairs=50)
    print("Analogy prediction accuracy on {}, embedding {} {}".format("Wordrep", ename, kk))

for embedding, ename in embeddings:
    
    kk = evaluate_on_semeval_2012_2(embedding)
    print("Analogy prediction accuracy on {}, embedding {} {}".format("Semeval", ename, kk))
