import nltk
from nltk.lm import NgramCounter
from nltk.util import ngrams
from collections import defaultdict
#nltk.download('punkt')
unigrams = defaultdict(lambda:0)
bigrams = defaultdict(lambda:0)


fid = open("microblog2011.txt")
fStopWords = open("StopWords.txt")

foutA = open('microblog2011_tokenized.txt','w')
foutB = open('Tokens.txt', 'w')
foutC1 = open('Words.txt', 'w')
foutC2 = open('WordsWithoutStops.txt', 'w')
foutD = open('BiWords.txt', 'w')

data = fid.read()
fid.close()

stopwords = fStopWords.read().split('\n')

tweets = data.split('\n')

print(f'tweet count:{len(tweets)}')

def tokenize(sent):
    sent = nltk.word_tokenize(sent)
    sent = map(lambda x:x.lower(), sent)
    return list(sent)
    

tokens = list( map(tokenize, tweets) )

for s in list(tokens):
    foutA.write(f'{s}\n')
foutA.close()

tokenCount = 0
for s in list(tokens):
    for i, w in enumerate(s):
        tokenCount += 1
        unigrams[w] += 1
        if i+1<len(s):
            bigrams[(s[i], s[i+1])] += 1

typeCount = len(unigrams.keys())

print(f'tokenCount:{tokenCount}, typeCount:{typeCount}')

print( f'typeCountRatio:{typeCount/tokenCount}' )

frequencies = list(unigrams.items())
frequencies.sort(key=lambda x:x[1], reverse=True)
for freq in frequencies:
    foutB.write(f'{freq}\n')
foutB.close()

onlyOnceCount = len( list( filter(lambda x: x[1]==1, frequencies) ) )
print(f'onlyOnceCount:{onlyOnceCount}')

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
# we define word to have at least one alphabet in them
def isWord(word):
    # words does not include '/'
    if '/' in word:
        return False
    # words must have at least one letter from alphabet
    hasAlpha = False
    hasNumeric = False
    for character in word:
        if character in alphabet:
            hasAlpha = True
        if character.isnumeric():
            hasNumeric = True
    return hasAlpha and not(hasNumeric)

stopwords.append('http')


frequencies = [(word, count) for (word, count) in frequencies if isWord(word)]
for freq in frequencies:
    foutC1.write(f'{freq}\n')
foutC1.close()

wordCount = sum([count for (word, count) in frequencies])
typeWordCount = len(frequencies)
print(f'wordCount:{wordCount}, typeWordCount:{typeWordCount}')
print( f'lexical density:{typeWordCount/wordCount}' )


wordCount = sum([count for (word, count) in frequencies])
typeWordCount = len(frequencies)
print('removing stopwords:')
print(f'wordCount:{wordCount}, typeWordCount:{typeWordCount}')
print( f'lexical density:{typeWordCount/wordCount}' )


frequencies = [(word, count) for (word, count) in frequencies if isWord(word) and not(word in stopwords)]
for freq in frequencies:
    foutC2.write(f'{freq}\n')
foutC2.close()



    
bifrequencies = list(bigrams.items())
bifrequencies.sort(key=lambda x:x[1], reverse=True)
bifrequencies = [((word1, word2), count) for ((word1, word2), count) in bifrequencies if isWord(word1) and isWord(word2) and not(word1 in stopwords) and not(word2 in stopwords)]
for freq in bifrequencies:
    foutD.write(f'{freq}\n')
foutD.close()
