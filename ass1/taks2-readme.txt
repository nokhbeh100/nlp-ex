This task requires word embeddings benchmarks library. It also requires gensim and bpemb which can be installed using pip.

To run the task:
python3 evaluate_similarity.py

dm.vec is the CBOW embedding we trained by ourselves.